integer Qchan=500001;			// channel used to query groups
integer Rchan=500002;	
integer Achan=500003;
integer QUnqGrp=50004;			// chan for querying one specific group.

integer ERR=-99;

default {
    state_entry() {
        llOwnerSay("Hello Scripter");
        	// we set our groupserver id for the api.
        	llMessageLinked(LINK_THIS, Achan, "10", NULL_KEY);	
    }
    
    touch_end(integer i) {
   		llMessageLinked(LINK_THIS, Qchan, "this is my test", llDetectedKey(0));
   		//to test a specific group:
   		// put the group uid in the string paramaters. It should be the firt one.
   				llMessageLinked(LINK_THIS, QUnqGrp, "123e4567-e89b-12d3-a456-426655440000,this is my test", llDetectedKey(0));	
    	//llParseString2List(str, ["|"], []);
    }
    
    link_message(integer source, integer num, string str, key result){
        if (num==Rchan) {
        	// str contains the string passed , if any...
        	llOwnerSay(str);
        	if (result!=NULL_KEY) {
        		llOwnerSay("welcome bro");
        	} else { 
        		llOwnerSay("kill him!");
        	}		
        }
    }
    
    
}
