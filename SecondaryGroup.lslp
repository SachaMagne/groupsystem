string RELEASE="R2.0/";
integer BcastChan = -12874587; // Totally random stuff
integer MainChan;
integer Timeout = 3;
integer GroupId=0;
integer AppId=5;

key server;
string RESIDENT_URL = "http://world.secondlife.com/group/";
string group;

// Global to reduce GC 
list token;
string tok;


Server(string txt){
	llRegionSayTo(server,BcastChan,txt);
}
integer ChanFromKey(key id){
	return  0x80000000 | ((integer)("0x"+(string)id) ^ AppId);	
}
default {
    state_entry() {
        llOwnerSay("Group system "+RELEASE);
        llSetMemoryLimit(llGetUsedMemory()+2048);
        llOwnerSay("Memory used:"+(string)(llGetUsedMemory()/1024)+" Kb");
 		
				 
        llListen(BcastChan+1,"",NULL_KEY,"");
        llHTTPRequest( RESIDENT_URL +(string) llGetObjectDetails(llGetKey(),[OBJECT_GROUP])
        			,[HTTP_METHOD,"GET"],"");
        
    }
    
     listen(integer chan,string name, key id,string message){
        // expect format:
        // Dest,xmit,token,data

        token=llCSV2List(message);
        tok= llList2String(token,0);
		server=id;

        if (tok=="GetGroup") {
        	GroupId=llList2Integer(token,1);
        	MainChan=llList2Integer(token,2);
        	llListen(MainChan+GroupId+1,"",NULL_KEY,"");
        	Server("GrpAdd,"
                +(string)llGetObjectDetails(llGetKey(),[OBJECT_GROUP])); 
            llSetText(group+" linked("+(string)GroupId+")",<0,1,0>,1);       
        }
        
        if (tok=="GrpBlg") {
 //       	"GrpBlg,"+(string)id+","+llList2String(token,1));
            integer r=llSameGroup(llList2Key(token,2));
            llRegionSayTo(llList2Key(token,1),MainChan+GroupId+1,"GrpReq,"+(string)r+","+llList2String(token,1));
          }
    }
    
    http_response(key request_id,integer status, list metadata, string body){
         integer start = llSubStringIndex(body,"<title>")+7;
         integer stop = llSubStringIndex(body,"</title")-1;
         string title=llGetSubString(body,start,stop);
         group=title; 
         llSetText(group+" unlinked.",<1,0,0>,1);        
    }
    
    on_rez(integer p){
    	llOwnerSay("[INFO] Please set my group like it should be.");
    	llOwnerSay("[INFO] then touch me to take it in account.");
    		
    }
    
    touch_end(integer i){
    	if (llDetectedKey(0) == llGetOwner()){
    		llOwnerSay("[INFO] Reseting group.");
    		llResetScript();	
    	}
    }
}
