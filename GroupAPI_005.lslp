 
integer Qchan=500001;			// channel used to query groups
integer Rchan=500002;			// Channel used to send the reply 
integer Achan=500003;			// chan for admin stuf
integer QUnqGrp=50004;			// chan for querying one specific group.

integer UpdateCache= 50006;
// heartbeat
integer Heartbeat = 50007;
integer HeartbeatR= 50008;

string API="5.0";

integer PongVal=0;

integer MainChan = -12874587;	// Communication to the groups system
integer chan;
integer timeout= 2;			// timeout in second.

integer OK=TRUE;
integer NOK=FALSE;
integer ERR=-99;
integer ServerId=1;

integer Result=FALSE;

string PreservedStr;

/*
 caching system.
 Works only for asking all the groups.
*/
list KCache=[];			// key
list KVal=[];			// status

integer AddKey=FALSE;
integer AppId=5;


integer CheckCache(key id, string str) {
	// first we check our cache 
    AddKey=TRUE;
    if (llGetFreeMemory()<2048) llResetScript();
    integer c=llListFindList(KCache, [id]);
    if (c != -1) {
        // we know that key and return the cached value.
        AddKey=FALSE;
        id=llList2Key(KVal,c);
        llMessageLinked(LINK_THIS, Rchan,str, id);
        return TRUE;	
    } 
    return FALSE;

}

integer ChanFromKey(key id){
	return  0x80000000 | ((integer)("0x"+(string)id) ^ AppId);	
}

default {
    state_entry() {
        llOwnerSay("Groups API ready");
        AddKey=FALSE;
          MainChan=ChanFromKey(
					llList2Key(llGetObjectDetails(llGetKey(), [OBJECT_GROUP]),0)
				);	
    }
    
    
    
    link_message(integer source, integer num, string str, key id){
         if (num==Qchan) {
        	
  			if (CheckCache(id,str) == TRUE) return;
  			
        	// time to ask if the passing key belong to a registered group.        
        	// we kept the link parameters;
        	PreservedStr=str; 
        	//
        	
 	      	llRegionSay(MainChan+ServerId,"GrpBlg,"+(string)id);
        	llSetTimerEvent(timeout);
        }
        
        // Dealing a single group query
         if (num==QUnqGrp) {
  //       	if (CheckCache(id,str) == TRUE) return;
        	
        	// time to ask if the passing key belong to a registered group.
        	// we kept the link parameters;
        	PreservedStr=str; 
        	//
        	key gid=llList2Key([str],0);
 	      	llRegionSay(MainChan+ServerId,"GrpUnk,"+(string)id+","+(string)gid);
        	llSetTimerEvent(timeout);
        }
        
       if (num==Achan) {
        	ServerId=(integer)str;
        	llOwnerSay("[GSAPI] Group set to "+(string)ServerId);
        	chan=llListen(MainChan+ServerId+1,"",NULL_KEY,"");	
        }
        
        
        if (num==UpdateCache) {
        	KCache=[];
        	KVal=[];	
        }
        
        
           
        if (num==Heartbeat) {	
        	llMessageLinked(LINK_THIS, HeartbeatR,(string)llGetFreeMemory(), NULL_KEY);
        }
        
    }
    
    
    timer(){
    	// we failed to have an answer on time.
    	llSetTimerEvent(0);
      	llMessageLinked(LINK_THIS, Rchan, PreservedStr, NULL_KEY);	
    }
    
    listen(integer chan, string name, key id, string message){
    	
   	 	
       key result;
   	 	
   	 	list token=llCSV2List(message);
   
        string tok= llList2String(token,0);
        
        if (tok=="ClrCache") {
        	KCache=[];
        	KVal=[];	
        }
        
        
        if (tok=="GrpReq") {
        	// we got an answer...
        	if (llList2Integer(token,1)==TRUE) {
        		
        		llSetTimerEvent(0);
        		if (llList2Integer(token,1)== TRUE){
               			  	result=id; 
               		
        		}else {
        			llOwnerSay("[GSAPI] No group found for "+llGetUsername(llList2Key(token,2)));
        				result=NULL_KEY;
        		}
        		if (AddKey==TRUE) {
    				KCache+=llList2Key(token,2);
    				KVal+=[result];	
    				AddKey=FALSE;
        		}
        		llMessageLinked(LINK_THIS, Rchan, PreservedStr, result);	
        	}
        }
        	
    }
    
}
