/*
Groupserver system
Allows scripts to check if someone is part of groups allowed to use it.

Rezz the server
Rezz a groupserver(s) and change their groups according your need.
Move them within 10m (whispers)


*/

string RELEASE="R2.0/";

integer BcastChan = -12874587; // Totally random stuff
integer MainChan;
integer AppId=5;

integer Timeout = 3;
list GroupName;
integer GroupCounter;

integer ServerId=1;
integer ExcludeMyGroup=FALSE;

string RESIDENT_URL = "http://world.secondlife.com/group/";

list AddGrp=[]; 		// key for additional group server found
list GrpUid=[];			// Gid of each group found;

list _menuItems = [];
integer _comChannel;
key _user;
integer _comHandle;
list _admin=[];

string configurationNotecardName = "Config";
key notecardQueryId;
integer line;

///////////////////////////////////////////////////////////////////////////
//    Copyright (C) 2013 Wizardry and Steamworks - License: GNU GPLv3    //
///////////////////////////////////////////////////////////////////////////
integer wasListCountExclude(list input, list exclude) {
    if(llGetListLength(input) == 0) return 0;
    if(llListFindList(exclude, (list)llList2String(input, 0)) == -1) 
        return 1 + wasListCountExclude(llDeleteSubList(input, 0, 0), exclude);
    return wasListCountExclude(llDeleteSubList(input, 0, 0), exclude);
}
 
///////////////////////////////////////////////////////////////////////////
//    Copyright (C) 2013 Wizardry and Steamworks - License: GNU GPLv3    //
///////////////////////////////////////////////////////////////////////////
list wasListMerge(list l, list m, string merge) {
    if(llGetListLength(l) == 0 && llGetListLength(m) == 0) return [];
    string a = llList2String(m, 0);
    if(a != merge) return [ a ] + wasListMerge(l, llDeleteSubList(m, 0, 0), merge);
    return [ llList2String(l, 0) ] + wasListMerge(llDeleteSubList(l, 0, 0), llDeleteSubList(m, 0, 0), merge);
}
 ///////////////////////////////////////////////////////////////////////////
//    Copyright (C) 2013 Wizardry and Steamworks - License: GNU GPLv3    //
///////////////////////////////////////////////////////////////////////////
integer wasMenuIndex = 0;
list wasEndlessMenu(list input, list actions, string direction) {
    integer cut = 11-wasListCountExclude(actions, [""]);
    if(direction == ">" &&  (wasMenuIndex+1)*cut+wasMenuIndex+1 < llGetListLength(input)) {
        ++wasMenuIndex;
        jump slice;
    }
    if(direction == "<" && wasMenuIndex-1 >= 0) {
        --wasMenuIndex;
        jump slice;
    }
@slice;
    integer multiple = wasMenuIndex*cut;
    input = llList2List(input, multiple+wasMenuIndex, multiple+cut+wasMenuIndex);
    input = wasListMerge(input, actions, "");
    return input;
}
 


AddGroup(string id){
    GroupCounter++;    
    llHTTPRequest( RESIDENT_URL + id,[HTTP_METHOD,"GET"],"");
}

integer ChanFromKey(key id){
	return  0x80000000 | ((integer)("0x"+(string)id) ^ AppId);	
}

 default {
    state_entry(){
    	llOwnerSay("[INFO] Group server "+RELEASE);
         llOwnerSay("[INFO] Group server starting...");
         llOwnerSay("[INFO] Checking for additional groups...");
        //we add ourself obviously...
      MainChan=ChanFromKey(
					llList2Key(llGetObjectDetails(llGetKey(), [OBJECT_GROUP]),0)
				);	
        state ReadConfig;
    }
        
}

state Config {
    state_entry() {
       // We query the groups
       GroupName=[];
       GroupCounter=0;
        llSetTimerEvent(Timeout);
        llListen(BcastChan,"",NULL_KEY,"");
 
        if (ExcludeMyGroup==FALSE){
         	AddGroup((string)llGetObjectDetails(llGetKey(),[OBJECT_GROUP]));
         	AddGrp=[llGetKey()];
         	GrpUid=[llList2Key(llGetObjectDetails(llGetKey(),[OBJECT_GROUP]),0)];
        }
         	
         llSensor("", NULL_KEY, PASSIVE|SCRIPTED, 3.0, PI);
      }
   
      sensor( integer detected ){
      	while (detected--){
      		llRegionSayTo(llDetectedKey(detected),BcastChan+1,llList2CSV(["GetGroup",ServerId,MainChan]));	
      	} 	
       	
      }
    
    listen(integer chan,string name, key id,string message){
       // We know who sent us a message from id...
       
        list token=llCSV2List(message);
 
        string tok= llList2String(token,0);
        if (tok=="GrpAdd") {
            AddGroup(llList2String(token,1)); 
            AddGrp+=[id];   
            GrpUid+=[llList2Key(token,1)];
        }
            
    }
    
    
    
    timer(){
        integer k;
        llSetTimerEvent(0);
        if (GroupCounter==1) {
            llOwnerSay("[INFO] No additional groups found.");
            llOwnerSay("[INFO] Permissions set to :"+llList2String(GroupName,0));    
        } else {
            llOwnerSay("[INFO] Additional group found:");
            for (k==0;k<=GroupCounter-1;k++){
                llOwnerSay("[GRP] "+llList2String(GroupName,k));    
            }        
        }
        state Running;
    }
    
     http_response(key request_id,integer status, list metadata, string body){
         integer start = llSubStringIndex(body,"<title>")+7;
         integer stop = llSubStringIndex(body,"</title")-1;
         string title=llGetSubString(body,start,stop);
         GroupName+=[title];        
    }
 
}



state Running {
    state_entry(){
        llOwnerSay("[INFO] System ready...");
        llListen(MainChan+ServerId,"",NULL_KEY,"");
        llSetText("Group server ("+(string)ServerId+") running.",<0,1,0>,1); 
        llSetTimerEvent(120);
    }    
    
    touch_end(integer i) {
    	_user=llDetectedKey(0);
    	
    	 if (((_user == llGetOwner()) || (llListFindList(_admin,(list)llDetectedName(0)) != -1))) {
    		state Menu;
    	}
    }
    
    
    listen(integer chan,string name, key id, string message){
    	list token=llCSV2List(message);
        
        integer i;
        string tok= llList2String(token,0);	
    	
    	 if (tok=="GrpBlg") {
    	 	
    	 	for (i=0;i<llGetListLength(AddGrp);i++) {
    	 		if (llList2Key(AddGrp,i)==llGetKey()){
    	 			// testing on our own group
    	 			integer r=llSameGroup(llList2Key(token,1));
           		 	llRegionSayTo(id,MainChan+ServerId+1,"GrpReq,"+(string)r+","+llList2String(token,1));	
    	 		}
    	 		llRegionSayTo(llList2Key(AddGrp,i),MainChan+ServerId+1,"GrpBlg,"+(string)id+","+llList2String(token,1));
    	 	}
    
          }
          
          // asking a specific group
          	 if (tok=="GrpUnk") {
    	 		integer r=llListFindList(GrpUid,[llList2Key(token,2)]);
    	 		if (r == -1) 
    	 			llRegionSayTo(id,MainChan+ServerId+1,"GrpReq,-1");
    	 			
    	 		llRegionSayTo(llList2Key(AddGrp,r),MainChan+ServerId+1,"GrpBlg,"+(string)id+","+llList2String(token,1));
    	 	}
    }
    
      changed(integer change){
        if(change & (CHANGED_OWNER | CHANGED_INVENTORY))
           llResetScript();
    }
    
    on_rez(integer start){
    	llResetScript();
    }
    
    timer(){
    if (llGetListLength(GrpUid)==1){
    	state Config;	
    	}
    }
    
}

state Menu{
	state_entry(){
		_comChannel = 10+(integer)llFrand(10);
		llSetTimerEvent(60);
		llListen(_comChannel,"",_user,"");
		_menuItems=["Scan","List","Show Admins","Clear cache","RESET"];
		llDialog(_user,"Group server "+(string)ServerId+"\n\nPlease select an action:\n",
					wasEndlessMenu(_menuItems, ["⏏ Exit"], ""),_comChannel);
		llSetText("Configuring Group server ("+(string)ServerId+")",<1,0,0>,1); 
	}	
	
	
	listen(integer channel, string name, key id, string message) {
		integer k;
		
		 if(message == "⏏ Exit") state Running;
		 if(message == "RESET") llResetScript();
		 if(message =="Scan") { state Config;}
		 if(message =="List") {
		 	llOwnerSay("[GRP] Group configured:");
			for (k==0;k<=GroupCounter-1;k++){
                llOwnerSay("[GRP] "+llList2String(GroupName,k));    
            }    	
		 }
		 if(message =="Show Admins") {
		 	llOwnerSay("[ADM] Administrator:");
			for (k==0;k<=llGetListLength(_admin);k++){
                llOwnerSay("[ADM] "+llList2String(_admin,k));    
            }    	
		 }
		 if (message == "Clear cache"){
		 	llRegionSay(MainChan+ServerId+1,"ClrCache");	
		 }
		 
		 
		 @remenu;
        llListenRemove(_comHandle);
        _comChannel = (integer)("0x8" + llGetSubString(llGetKey(), 0, 6));
        _comHandle = llListen(_comChannel, "", _user, "");
      llDialog(_user,"Group server "+(string)ServerId+"\n\nPlease select an action:\n",
      				wasEndlessMenu(_menuItems, ["⏏ Exit"], ""),_comChannel);

		
	}
	timer(){
		llSetTimerEvent(0);
		state Running;	
	}
}

state ReadConfig {
	state_entry(){
		if(llGetInventoryType(configurationNotecardName) != INVENTORY_NOTECARD){
        llOwnerSay("[ERR] Missing inventory notecard: " + configurationNotecardName);
        state Config;
    }
    line = 0;
    notecardQueryId = llGetNotecardLine(configurationNotecardName, line);
	}	
	
	 dataserver(key query_id, string data) {
        if(query_id == notecardQueryId) {
        	 if(data == EOF) {
        	 	llOwnerSay("[INFO] Configuration read.");
  				state Config;  
    		}
    		if (data !="") {
    			if(llSubStringIndex(data, "#") != 0){
    			  integer i = llSubStringIndex(data, "=");
    			     if(i != -1){
    		     		string name = llGetSubString(data, 0, i - 1);
 		               	string value = llGetSubString(data, i + 1, -1);
                		list temp = llParseString2List(name, [" "], []);
                		name = llDumpList2String(temp, " ");
                		name = llToLower(name);
                		temp = llParseString2List(value, [" "], []);
                		value = llDumpList2String(temp, " ");
    			     	
    			     	//llOwnerSay("Reading line :"+data);
    			     	if (name=="serverid") ServerId=(integer)value;
    			     	
    			     	if (name=="admin") _admin+=[value];
    			     	
    			     	if (name=="excludemygroup") ExcludeMyGroup=(integer)value;
    			     	
    			     } // kvp found
    			  	
    			} // no comment
    			
    			
    		} // end of not empty line 
    		 notecardQueryId = llGetNotecardLine(configurationNotecardName, ++line);
        }
    }
	
}
