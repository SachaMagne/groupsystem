Group system: Release 2.1

HOWTO
=====
Rezz the main group server,
modify the Configuration if needed (see content) 
Rezz one or several additional group  and set their groups accordingly. they should be within 1m range, then click on it to initialize it.
Click the server and select Scan to start the configuration.

Configuration card
==================

Keyword 		content
Admin			login name of your admins, one per admin line
ServerId		Id of the actual server, default 1 
ExcludeMyGroup	True will exclude the server's rezzed group from any queries


Integration on any external scripts
====================================
Once the server and additional groups rezzed and configured,
insert the script API_002 into the object who need to check the groups
and adapt the demo script to fit your need.

REMARQ
=======
Be sure to have all the system setup with the good ID!



